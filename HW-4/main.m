//
//  main.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 ResolveR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
