//
//  CountryViewController.h
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryModel.h"

@protocol sendCountry <NSObject>

-(void)sendCountry:(NSString *)country;

@end

@interface CountryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(strong,nonatomic) CountryModel* modelCountries;//our countries
@property (nonatomic, strong) IBOutlet UITableView *tableView;//it is table view =cap
@property (nonatomic, strong) NSMutableArray *findRes;//find result massiv
@property (nonatomic,strong) id<sendCountry> delegate;//delegate for input dannie


@end
