//
//  EditViewController.h
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryViewController.h"
@protocol sendUserData <NSObject>

-(void)sendData:(NSMutableDictionary *)userData;

@end

@interface EditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *secondName;
@property (weak, nonatomic) IBOutlet UITextField *middleName;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UILabel *country;
@property (strong, nonatomic) IBOutlet UIButton *Image;

@property (strong,nonatomic) NSMutableDictionary *userData;
@property (assign,nonatomic)id delegate;

- (IBAction)Done:(UIBarButtonItem *)sender;

@end
