//
//  ViewController.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//  Thanks Ivan Perelehov for help

#import "ViewController.h"

@implementation ViewController


//return void////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
    [self loadDataFromFile];
    [self loadDataToView];
}
-(void)safeDataToFile
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *userDataName = @"UserData";
    [self.userData writeToFile:[path stringByAppendingPathComponent:userDataName] atomically:YES];
}

-(void)sendData:(NSMutableDictionary *)userData
{
    self.userData=userData;
    [self safeDataToFile];
}

-(void)loadDataFromFile
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = @"Avatar";
    NSString *userDataName = @"UserData";
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[path stringByAppendingPathComponent:imageName]])
    {
        NSData *data= [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:imageName]];
        UIImage *img=[[UIImage alloc] initWithData:data];
        self.PhotoImage.image = img;
    }
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[path stringByAppendingPathComponent:userDataName]])
    {
        self.userData=[[NSMutableDictionary alloc]initWithContentsOfFile:[path stringByAppendingPathComponent:userDataName]];
    }
    else
    {
        _userData = [[NSMutableDictionary alloc] init];
    }
}


-(void)loadDataToView
{
    self.firstName.text = [_userData valueForKey:@"firstName"];
    self.secondName.text = [_userData valueForKey:@"secondName"];
    self.middleName.text = [_userData valueForKey:@"middleName"];
    self.phone.text = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"Phone"]];
    self.country.text = [NSString stringWithFormat:@"%@",[_userData valueForKey:@"country"]];
    self.email.text= [NSString stringWithFormat:@"%@",[_userData valueForKey:@"email"]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    [self loadDataToView];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = @"Avatar";
    if([[NSFileManager defaultManager] fileExistsAtPath:[path stringByAppendingPathComponent:imageName]]){
        NSData *data= [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:imageName]];
        UIImage *img=[[UIImage alloc] initWithData:data];
        self.PhotoImage.image = img;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if([[segue destinationViewController] isKindOfClass:[EditViewController class]]){
        EditViewController* dest=[segue destinationViewController];
        dest.userData=self.userData;
        dest.delegate=self;
    }
    
}

@end
