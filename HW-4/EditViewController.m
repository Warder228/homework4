//
//  EditViewController.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import "EditViewController.h"
#import "ViewController.h"

@interface EditViewController ()<sendCountry>

@end

@implementation EditViewController

-(void)sendCountry:(NSString *)country
{
    self.country.text=country;
    [_userData removeObjectForKey:country];
    [_userData setValue:country forKey:@"country"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadDataToView];
    [self.firstName setHighlighted:YES];
}

-(void)loadDataFromView
{
    [_userData removeAllObjects];
    [_userData setValue:self.firstName.text forKey:@"firstName"];
    [_userData setValue:self.secondName.text forKey:@"secondName"];
    [_userData setValue:self.middleName.text forKey:@"middleName"];
    [_userData setValue:self.phone.text forKey:@"Phone"];
    [_userData setValue:self.email.text forKey:@"email"];
    [_userData setValue:self.country.text forKey:@"country"];
}

-(void)loadDataToView
{
    self.firstName.text = [_userData valueForKey:@"firstName"];
    self.secondName.text = [_userData valueForKey:@"secondName"];
    self.middleName.text = [_userData valueForKey:@"middleName"];
    self.phone.text = [_userData valueForKey:@"Phone"];
    self.country.text = [_userData valueForKey:@"country"];
    self.email.text= [_userData valueForKey:@"email"];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = @"Avatar";
    if([[NSFileManager defaultManager] fileExistsAtPath:[path stringByAppendingPathComponent:imageName]]){
        NSData *data= [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:imageName]];
        UIImage *img=[[UIImage alloc] initWithData:data];
        [self.Image setBackgroundImage:img forState:UIControlStateNormal];
    }
}

-(BOOL)checkData
{
    NSString *emailRegex= @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    
    if([self.firstName.text isEqualToString:@""]){
        [self.firstName setBackgroundColor:[UIColor redColor]];
        return NO;
    }
    else{
        [self.firstName setBackgroundColor:[UIColor whiteColor]];
    }
    
    if([self.secondName.text isEqualToString:@""]){
        [self.secondName setBackgroundColor:[UIColor redColor]];
        return NO;
    }
    else{
        [self.secondName setBackgroundColor:[UIColor whiteColor]];
    }
    
    if([self.middleName.text isEqualToString:@""]){
        [self.middleName setBackgroundColor:[UIColor redColor]];
        return NO;
    }
    else{
        [self.middleName setBackgroundColor:[UIColor whiteColor]];
    }
    
    if(![emailPredicate evaluateWithObject:self.email.text]){
        [self.email setBackgroundColor:[UIColor redColor]];
        return NO;
    }
    else{
        [self.email setBackgroundColor:[UIColor whiteColor]];
    }
    
    if(![phonePredicate evaluateWithObject:self.phone.text]){
        [self.phone setBackgroundColor:[UIColor redColor]];
        return NO;
    }
    else{
        [self.phone setBackgroundColor:[UIColor whiteColor]];
    }
    
    return YES;
}


- (IBAction)Done:(UIBarButtonItem *)sender
{
    [self loadDataFromView];
    if([self checkData]){
        [self.delegate sendData:_userData];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self loadDataFromView];
    if([[segue destinationViewController] isKindOfClass:[EditViewController class]])
    {
        EditViewController* dest = [segue destinationViewController];
        dest.userData=self.userData;
        dest.delegate=self;
    }
    
    if([[segue destinationViewController] isKindOfClass:[CountryViewController class]])
    {
        CountryViewController* dest = [segue destinationViewController];
        dest.delegate=self;
    }
}
@end
