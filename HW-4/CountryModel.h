//
//  CountryModel.h
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryModel : NSObject

@property (strong, nonatomic) NSMutableArray *countries;
@property (strong,nonatomic) NSMutableArray *code;

@end
