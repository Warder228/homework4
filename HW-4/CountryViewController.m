//
//  CountryViewController.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import "CountryViewController.h"
#import "TableViewCell.h"
#import <UIKit/UIKit.h>

@interface CountryViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchDisplayDelegate,UISearchBarDelegate>

@end

@implementation CountryViewController

//переопределим методы

//return void ////////////////////////////////////////
- (void)viewDidLoad
{
    [super viewDidLoad];
    _modelCountries=[[CountryModel alloc] init];
    self.findRes = [NSMutableArray arrayWithArray:self.modelCountries.countries];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate sendCountry:[self.findRes objectAtIndex:indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)searchBar:(UISearchBar * _Nonnull)searchBar textDidChange:(NSString * _Nonnull)searchText
{
    [self.findRes removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
    self.findRes = [NSMutableArray arrayWithArray: [self.modelCountries.countries filteredArrayUsingPredicate:resultPredicate]];
    [self.tableView reloadData];
}



//return view ////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell= [tableView dequeueReusableCellWithIdentifier:@"cell" ];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.textLabel.text = [self.findRes objectAtIndex:indexPath.row];
    return cell;
}

//return int ////////////////////////////////////////
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.findRes count];
}


@end
