//
//  ImageViewController.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageModel.h"
#import "ImageCollectionViewCell.h"
#import <Photos/Photos.h>

NSString * const ImageViewControllerCellRuseIdentifier = @"ImageViewControllerCellRuseIdentifier";

@interface ImageViewController ()<UICollectionViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegate, ImageModelDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) ImageModel *model;

@end

@implementation ImageViewController

//ret void////////////////////////////////////////
-(void) didUpdateData
{
    [_collectionView reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell *cell = (ImageCollectionViewCell*) [collectionView cellForItemAtIndexPath:indexPath];
    if(cell)
    {
        [self.model saveUserImage:cell.imageView.image];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.model = [[ImageModel alloc] init];

    self.collectionView.dataSource = self;
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo NS_DEPRECATED_IOS(2_0, 3_0)
{
    [self.model saveUserImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

//ret view////////////////////////////////////////
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell"  forIndexPath:indexPath];
    
    id item = [self.model itemForIndexPath:indexPath];
    
    if ([item isKindOfClass:[PHAsset class]])
    {
        PHAsset *asset = (PHAsset *)item;
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:CGSizeMake(150.0f, 150.f)
                                                  contentMode:PHImageContentModeDefault
                                                      options:nil
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                    cell.imageView.image = result;
                                                }];
    }
    if ([item isKindOfClass:[NSString class]])
    {
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *lastPathComponent = item;
        cell.imageView.image = [UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:lastPathComponent]];
    }
    
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld",(long)indexPath.item]];
    
    return cell;
}


//ret Action////////////////////////////////////////

- (IBAction)Photo:(UIBarButtonItem *)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}


//ret int/////////////////////////////////////////

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.model sectionsCount];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.model countForSection:section];
}

@end

