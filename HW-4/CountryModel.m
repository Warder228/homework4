//
//  CountryModel.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import "CountryModel.h"

@implementation CountryModel

-(instancetype)init
{
    self = [super init];
    
    _countries = [[NSMutableArray alloc] init];
    NSLocale *curLocale = [NSLocale currentLocale];
    
    for(NSString *code in [NSLocale ISOCountryCodes])
    {
        NSString *localizedCountry = [curLocale localizedStringForCountryCode:code];
        if(localizedCountry)
        {
            [_countries addObject:localizedCountry];
            [_code addObject:code];
        }
    }
    return self;
}

@end
