//
//  ViewController.h
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//  

#import <UIKit/UIKit.h>
#import "EditViewController.h"
#import "CountryViewController.h"

@interface ViewController : UIViewController <sendUserData>

@property (strong, nonatomic) IBOutlet UILabel *secondName;
@property (strong, nonatomic) IBOutlet UILabel *firstName;
@property (strong, nonatomic) IBOutlet UILabel *middleName;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *phone;
@property (strong, nonatomic) IBOutlet UILabel *country;
@property (weak, nonatomic) IBOutlet UIImageView *PhotoImage;

@property (strong,nonatomic) NSMutableDictionary* userData;

-(void)sendData:(NSMutableDictionary *)userData;

@end
