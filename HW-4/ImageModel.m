//
//  ImageModel.m
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import "ImageModel.h"
#import <Photos/Photos.h>

NSString *const ImagesFilename = @"ImageModelItems";

@interface ImageModel () <PHPhotoLibraryChangeObserver>

@property (nonatomic, strong) NSMutableArray *mutableImages;
@property (nonatomic, strong) PHFetchResult *fetchResult;

@end

@implementation ImageModel

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
        _fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    }
    return self;
}


///return void////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)photoLibraryDidChange:(PHChange *)changeInstance
{
    PHFetchResultChangeDetails *details = [changeInstance changeDetailsForFetchResult:self.fetchResult];
    
    if(self.delegate)
    {
        [self.delegate didUpdateData];
    }
    if (details)
    {
        self.fetchResult = details.fetchResultAfterChanges;
    }
}


-(void)saveUserImage:(UIImage *)image
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = @"Avatar";
    if(![[NSFileManager defaultManager] fileExistsAtPath:[path stringByAppendingPathComponent:imageName]])
        NSLog(@"No file");
    [UIImagePNGRepresentation(image) writeToFile:[path stringByAppendingPathComponent:imageName] atomically:YES];
}

- (void)saveImage:(UIImage *)image
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = [NSUUID UUID].UUIDString;
    
    [UIImagePNGRepresentation(image) writeToFile:[path stringByAppendingPathComponent:imageName]
                                      atomically:YES];
    [self.mutableImages addObject:imageName];
    
    [NSKeyedArchiver archiveRootObject:self.mutableImages toFile:[path stringByAppendingPathComponent:@"ImagesFilename"]];
}



//for view -otobrazhenie////////////////////////////////////////////////////
- (id)itemForIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.section == 0) || (indexPath.section == 1))
        return self.fetchResult[indexPath.item];
    return nil;
}

//ret int////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)sectionsCount
{
    return 1;
}

- (NSInteger)countForSection:(NSInteger)section
{
    if ((section == 0) || (section == 1))
        return self.fetchResult.count;
    return 0;
}


@end
