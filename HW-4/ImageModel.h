//
//  ImageModel.h
//  HW-4
//
//  Created by Admin on 19.11.16.
//  Copyright © 2016 Warder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ImageModelDelegate <NSObject>

-(void)didUpdateData;

@end

@interface ImageModel : NSObject

@property (nonatomic,weak) id<ImageModelDelegate> delegate;


- (void)saveImage:(UIImage *)image;

- (void)saveUserImage:(UIImage *)image;

- (id)itemForIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)sectionsCount;

- (NSInteger)countForSection:(NSInteger)section;

@end
